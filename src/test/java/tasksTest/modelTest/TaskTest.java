package tasksTest.modelTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {
    Task task;
    Task task_empty;
    Task task_active;
    Date yesterday = new Date(System.currentTimeMillis() - 24*60*60*1000);
    Date tomorrow = new Date(System.currentTimeMillis() + 24*60*60*1000);
    Date now = new Date(System.currentTimeMillis());
    Date next = new Date(now.getTime() + 30 * 1000);
    Date next_la_next = new Date(now.getTime() + 30 * 1000*2);
    Task task_active_not_repeated;
    @BeforeEach
    public void setup(){
        task = new Task("title", new Date(System.currentTimeMillis()));
        task_active = new Task(now,now,now,30,true);
        task_empty = new Task(now,now,tomorrow,30,true);
        task_active_not_repeated = new Task(now,now,tomorrow,0,true);
    }
    @Test
    void should_ReturnNull_WhenTaskNotActiveAndRepeated() {
        assertNull(task.nextTimeAfter(new Date()));
    }

    @Test
    void should_ReturnStart_WhenCurrentBeforeStart() {
        assertTrue(task_active.nextTimeAfter(yesterday).equals(task_active.getStartTime()));
    }

    @Test
    void should_ReturnNull_WhenTaskRepeatedAndActiveAndNotBeforeStartOrEnd() {
        assertNull(task_active.nextTimeAfter(tomorrow));
    }

    @Test
    void should_ReturnNull_WhenTaskRepeatedAndActiveAndOutsideStartAndEnd() {
        assertNull(task_active.nextTimeAfter(now));
    }

    @Test
    void should_ReturnNext_WhenTaskRepeatedAndActiveAndEqualToStartPlusInterval() {
        assertTrue(task_empty.nextTimeAfter(next).equals(next_la_next));
    }

    @Test
    void should_ReturnNext_WhenTaskRepeatedAndActiveAndBetweenStartAndEnd() {
        assertTrue(task_empty.nextTimeAfter(new Date(next.getTime() - 1)).equals(next));
    }

    @Test
    void should_ReturnTime_WhenTaskNotRepeatedAndActiveAndCurrentBeforeTime() {
        assertTrue(task_active_not_repeated.nextTimeAfter(new Date(now.getTime() - 1)).equals(now));
    }
}