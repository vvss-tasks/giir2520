package tasksTest.repositoryTest;

import org.junit.jupiter.api.*;
import tasksTest.modelTest.Task;
import tasksTest.repositoryTest.data.ArrayTaskList;
import tasksTest.repositoryTest.data.TaskList;
import tasksTest.view.Main;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("[Repository test] TaskIO")
class TaskIOTest {

    TaskIO taskIO;
    Task task;
    TaskList result;

    // arrange
    @BeforeEach
    public void setUp() throws Exception {
        this.taskIO = new TaskIO();
        this.task = new Task("Some title", new Date());
        this.result = new ArrayTaskList();
    }

    @Test
    @Timeout(1000)
    @Order(1)
    @DisplayName("[writeBinary] taskList correctly initialized, file is null.")
    @Tag("file")
    void should_ThrowNullPointerException_WhenFileIsNull() {
        try {
            // action
            TaskIO.writeBinary(new ArrayTaskList(), null);

            // assert
            assert (false);
        } catch (IOException | NullPointerException e) {
            assert (true);
        }
    }

    @Test
    @Timeout(1000)
    @Order(2)
    @DisplayName("[writeBinary] params are null.")
    @Tag("params")
    void should_ThrowException_WhenParamsAreNull() {
        try {
            // action
            TaskIO.writeBinary(null, null);

            // assert
            assert (false);
        } catch (IOException | NullPointerException e) {
            assert (true);
        }
    }

    @Test
    @Timeout(1000)
    @Order(3)
    @DisplayName("[writeBinary] taskList is null, file correctly initialized.")
    @Tag("taskList")
    void should_ThrowNullPointerException_WhenTaskListIsNull() {
        try {
            // action
            TaskIO.writeBinary(null, Main.testTasksFile);

            // assert
            assert (false);
        } catch (IOException | NullPointerException e) {
            assert (true);
        }
    }

    @Test
    @Timeout(5000)
    @Order(4)
    @DisplayName("[writeBinary] params correctly initialized.")
    @Tag("params")
    void should_WriteTaskList_WhenParamsGood() {
        try {
            // arange
            TaskList tasksToBeWritten = new ArrayTaskList();
            tasksToBeWritten.add(task);

            // action
            TaskIO.writeBinary(tasksToBeWritten, Main.testTasksFile);

            //assert
            TaskIO.readBinary(result, Main.testTasksFile);
            assertEquals(1, result.getAll().size());
            assertEquals(task, result.getAll().get(0));
        } catch (IOException | NullPointerException e) {
            assert (false);
        }
    }

    @Test
    @Timeout(5000)
    @Order(5)
    @DisplayName("[writeBinary] taskList is empty.")
    @Tag("taskList")
    void should_WriteTaskList_WhenTaskListIsEmpty() {
        try {
            // arrange
            result = new ArrayTaskList();

            // action
            TaskIO.writeBinary(new ArrayTaskList(), Main.testTasksFile);

            //assert
            TaskIO.readBinary(result, Main.testTasksFile);
            assertEquals(0, result.getAll().size());

        } catch (IOException | NullPointerException e) {
            assert (false);
        }
    }


    @Test
    @Timeout(5000)
    @Order(6)
    @DisplayName("[writeBinary] taskList contains null objects.")
    @Tag("taskList")
    void should_ThrowNullPointerException_WhenTaskListHasNullTasks() {
        try {
            // arange
            TaskList tasksToBeWritten = new ArrayTaskList();
            tasksToBeWritten.add(null);
            tasksToBeWritten.add(null);
            tasksToBeWritten.add(null);

            // action
            TaskIO.writeBinary(tasksToBeWritten, Main.testTasksFile);

            //assert
            TaskIO.readBinary(result, Main.testTasksFile);
            assert (false);

        } catch (IOException | NullPointerException e) {
            assert (true);
        }
    }

    @Test
    @Timeout(5000)
    @Order(7)
    @DisplayName("[writeBinary] file does not exist.")
    @Tag("file")
    void should_ThrowNullPointerException_WhenNonExistingFile() {
        try {
            // arange
            TaskList tasksToBeWritten = new ArrayTaskList();

            // action
            TaskIO.writeBinary(tasksToBeWritten, new File(Main.class.getClassLoader().getResource("data/nonExisting.txt").getFile()));

            //assert
            assert (false);

        } catch (IOException | NullPointerException e) {
            assert (true);
        }
    }
}