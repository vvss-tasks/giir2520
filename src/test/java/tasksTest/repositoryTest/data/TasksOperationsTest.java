package tasksTest.repositoryTest.data;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasksTest.modelTest.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class TasksOperationsTest {

    public ArrayList<Task> tasks = new ArrayList<>();
    public TasksOperations repo = new TasksOperations(FXCollections.observableArrayList());
    public Date date = new Date();
    public static final long HOUR = 3600 * 1000;

    @BeforeEach
    public void setup() {
        Task t1 = mock(Task.class);
        Task t2 = mock(Task.class);
        tasks.add(t1);
        tasks.add(t2);
    }

    @Test
    void should_returnAllTasks_WhenTasksNextTimeAfterIsBeforeEnd() {
        TasksOperations sRepo = Mockito.spy(repo);
        Task t = mock(Task.class);
        Mockito.when(t.nextTimeAfter(any())).thenReturn(new Date());
        tasks.add(t);
        sRepo.tasks = tasks;

        Iterator<Task> iterator = sRepo.incoming(new Date(), new Date(date.getTime() + 2 * HOUR)).iterator();

        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), t);
        assertFalse(iterator.hasNext());
    }

    @Test
    void should_returnEmptyList_WhenNoTasksNextTimeAfterIsBeforeEnd() {
        TasksOperations sRepo = Mockito.spy(repo);
        sRepo.tasks = tasks;

        Iterator<Task> iterator = sRepo.incoming(new Date(), new Date(date.getTime() + 2 * HOUR)).iterator();

        assertFalse(iterator.hasNext());
    }

}