package tasksTest.integration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasksTest.modelTest.Task;
import tasksTest.repositoryTest.data.LinkedTaskList;
import tasksTest.repositoryTest.data.TaskList;
import tasksTest.repositoryTest.data.TasksOperations;
import tasksTest.services.TasksService;

import java.util.Date;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class IntegrationSRETest {

    public static final Date DATE = new Date();
    public static final long HOUR = 3600 * 1000;
    public static final Date YESTERDAY = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);

    TasksOperations repo = new TasksOperations();
    TasksService service;
    Task t1 = new Task(DATE, DATE, DATE, 30, true);
    Task t2 = new Task(DATE, DATE, DATE, 50, true);

    @BeforeEach
    public void setup() {
        TaskList l = new LinkedTaskList();
        l.add(t1);
        l.add(t2);
        service = new TasksService(l, repo);
    }

    @Test
    void should_returnAllTasks_whenTasksMatchStartAndEndDateCondition() {
        Iterator<Task> iterator = service.filterTasks(YESTERDAY, new Date(DATE.getTime() + 2 * HOUR)).iterator();

        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), t1);
        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), t2);
        assertFalse(iterator.hasNext());
    }

    @Test
    void should_returnEmptyList_whenNoTasksMatchStartAndEndDateCondition() {
        Iterator<Task> iterator = service.filterTasks(new Date(), new Date(DATE.getTime() - 2 * HOUR)).iterator();

        assertFalse(iterator.hasNext());
    }
}