package tasksTest.integration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasksTest.modelTest.Task;
import tasksTest.repositoryTest.data.LinkedTaskList;
import tasksTest.repositoryTest.data.TaskList;
import tasksTest.repositoryTest.data.TasksOperations;
import tasksTest.services.TasksService;

import java.util.Date;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class IntegrationSRTest {

    TasksOperations repo = new TasksOperations();
    TasksService service;
    public static final Date DATE = new Date();
    public static final long HOUR = 3600 * 1000;
    Task t1 = mock(Task.class);
    Task t2 = mock(Task.class);

    @BeforeEach
    public void setup() {
        Mockito.when(t1.nextTimeAfter(any())).thenReturn(new Date());
        Mockito.when(t2.nextTimeAfter(any())).thenReturn(new Date());
        TaskList l = new LinkedTaskList();
        l.add(t1);
        l.add(t2);
        service = new TasksService(l, repo);
    }

    @Test
    void should_returnAllTasks_whenTasksMatchStartAndEndDateCondition() {
        Iterator<Task> iterator = service.filterTasks(new Date(), new Date(DATE.getTime() + 2 * HOUR)).iterator();

        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), t1);
        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), t2);
        assertFalse(iterator.hasNext());
    }

    @Test
    void should_returnEmptyList_whenNoTasksMatchStartAndEndDateCondition() {
        Iterator<Task> iterator = service.filterTasks(new Date(), new Date(DATE.getTime() - 2 * HOUR)).iterator();

        assertFalse(iterator.hasNext());
    }
}