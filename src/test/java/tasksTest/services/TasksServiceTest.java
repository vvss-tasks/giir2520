package tasksTest.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasksTest.modelTest.Task;
import tasksTest.repositoryTest.data.LinkedTaskList;
import tasksTest.repositoryTest.data.TasksOperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class TasksServiceTest {


    TasksOperations repo = mock(TasksOperations.class);

    TasksService service = new TasksService(new LinkedTaskList(), repo);

    @BeforeEach
    void setUp() {
    }

    @Test
    void should_returnAllTasks_whenTasksMatchStartAndEndDateCondition() {
        Task t1 = mock(Task.class);
        Task t2 = mock(Task.class);
        Mockito.when(repo.incoming(any(), any())).thenReturn(Arrays.asList(t1, t2));

        Iterator<Task> iterator = service.filterTasks(new Date(), new Date()).iterator();

        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), t1);
        assertTrue(iterator.hasNext());
        assertEquals(iterator.next(), t2);
        assertFalse(iterator.hasNext());
    }

    @Test
    void should_returnEmptyList_whenNoTasksMatchStartAndEndDateCondition() {
        Mockito.when(repo.incoming(any(), any())).thenReturn(new ArrayList<>());

        Iterator<Task> iterator = service.filterTasks(new Date(), new Date()).iterator();

        assertFalse(iterator.hasNext());
    }
}