package tasksTest.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tasksTest.modelTest.Task;
import tasksTest.repositoryTest.data.TaskList;
import tasksTest.repositoryTest.data.TasksOperations;

import java.util.Date;

public class TasksService {

    private ObservableList<Task> obsTaskList;
    private TasksOperations repo;

    public TasksService(TaskList tasks, TasksOperations repo) {
        obsTaskList = FXCollections.observableArrayList(tasks.getAll());
        this.repo = repo;
    }

    public ObservableList<Task> getObservableList() {
        return obsTaskList;
    }

    public String getIntervalInHours(Task task) {
        int seconds = task.getRepeatInterval();
        int minutes = seconds / DateService.SECONDS_IN_MINUTE;
        int hours = minutes / DateService.MINUTES_IN_HOUR;
        minutes = minutes % DateService.MINUTES_IN_HOUR;
        return formTimeUnit(hours) + ":" + formTimeUnit(minutes);//hh:MM
    }

    public String formTimeUnit(int timeUnit) {
        StringBuilder sb = new StringBuilder();
        if (timeUnit < 10) sb.append("0");
        if (timeUnit == 0) sb.append("0");
        else {
            sb.append(timeUnit);
        }
        return sb.toString();
    }


    public int parseFromStringToSeconds(String stringTime) {//hh:MM
        String[] units = stringTime.split(":");
        int hours = Integer.parseInt(units[0]);
        int minutes = Integer.parseInt(units[1]);
        int result = (hours * DateService.MINUTES_IN_HOUR + minutes) * DateService.SECONDS_IN_MINUTE;
        return result;
    }

    public Iterable<Task> filterTasks(Date start, Date end) {
        repo.reset(getObservableList());
        Iterable<Task> filtered = repo.incoming(start, end);
        return filtered;
    }
}
